#!/usr/bin/env sh

TABLE_AGENT='pl.edu.pw.waitersys.TableAgent'
WAITER_AGENT='pl.edu.pw.waitersys.WaiterAgent'
LAUNCHED_AGENTS="\
table1:$TABLE_AGENT;\
table2:$TABLE_AGENT;\
table3:$TABLE_AGENT;\
waiter1:$WAITER_AGENT;\
waiter2:$WAITER_AGENT\
"

CLASSPATH=`./gradlew -q classpath`

mkdir -p run && cd run

java -cp $CLASSPATH jade.Boot -agents $LAUNCHED_AGENTS
