package pl.edu.pw.waitersys;

import jade.core.Agent;
import pl.edu.pw.waitersys.directory.WaiterDirectory;

/**
 * Agent reprezentujący interesy kelnera.
 */
public class WaiterAgent extends Agent {
    /**
     * Inicjalizuje agenta: rejestruje go w katalogu kelnerów.
     */
    protected void setup() {
        System.out.println("Przyszedł nowy kelner! Identyfikator: " + getAID().getName());

        // Zarejestruj się w katalogu kelnerów.
        WaiterDirectory.announce(this);
    }

    /**
     * Obsługuje śmierć agenta: wyrejestrowuje go z katalogu kelnerów.
     */
    protected void takeDown() {
        System.out.println("Kelnera " + getAID().getName() + " opuścił lokal.");

        // Wyrejestruj się z katalogu kelnerów.
        WaiterDirectory.renounce(this);
    }
}
