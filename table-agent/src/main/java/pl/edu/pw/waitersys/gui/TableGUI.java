package pl.edu.pw.waitersys.gui;

import pl.edu.pw.waitersys.Food;
import pl.edu.pw.waitersys.Order;
import pl.edu.pw.waitersys.TableAgent;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Proste GUI związane z wybranym agentem stolikowym, pozwalające na wybór zamówienia.
 *
 * TODO W tej chwili zamówienie tworzone jest dla każdej klikniętej potrawy z osobna.
 *      Warto byłoby obsłużyć dodawanie i usuwanie potraw z zamówienia.
 */
public class TableGUI extends Frame {
    /**
     * Konstruuje GUI. Okno jest nierozerwalnie związane z podanym agentem.
     *
     * Okno nie jest wyświetlane bezpośrednio po skonstruowaniu. Aby było widoczne,
     * należy jawnie wywołać metodę setVisible(true)!.
     *
     * @param owner     Agent będący właścicielem GUI.
     * @param tableName Nazwa stolika wyświetlana jako tytuł okna.
     */
    public TableGUI(TableAgent owner, final String tableName) {
        super(tableName);

        this.owner = owner;

        final int numPositions = Food.values().length;

        setSize(POSITION_WIDTH, numPositions * POSITION_HEIGHT);
        setLayout(new GridLayout(numPositions, 1));

        addWindowListener(new CloseHandler());

        addPositionButtons(Food.values());
    }

    /**
     * Dodaje do okna przyciski odpowiadające podanym potrawom.
     *
     * @param foods Potrawy, które powinny być uwzględnione w klikalnym menu.
     */
    private void addPositionButtons(Food[] foods) {
        for (Food food : foods) {
            Button button = new Button(food.readableName());
            button.addActionListener(new FoodClickHandler(food));
            add(button);
        }
    }

    /**
     * Obsługuje zdarzenia lifecycle'owe okna.
     */
    private class CloseHandler extends WindowAdapter {
        /**
         * Obsługuje prośbę o zamknięcie okna (na przykład po kliknięciu X).
         * Zabija agenta-właściciela, a później samo okno.
         */
        public void windowClosing(WindowEvent event) {
            owner.doDelete();
            dispose();
        }
    }

    /**
     * Obsługuje kliknięcia w przycisk związany z wybraną potrawą.
     * Powiadamia agenta o wybranym zamówieniu.
     *
     * TODO W tej chwili natychmiast po kliknięciu tworzone jest nowe zamówienie.
     */
    private class FoodClickHandler implements ActionListener {
        FoodClickHandler(final Food handledFood) {
            this.handledFood = handledFood;
        }

        public void actionPerformed(ActionEvent e) {
            final Order order = new Order(handledFood);
            owner.handleOrder(order);
        }

        private final Food handledFood;
    }

    /**
     * Szerokość przycisku z potrawą.
     */
    private final static int POSITION_WIDTH  = 250;

    /**
     * Wysokość przycisku z potrawą.
     */
    private final static int POSITION_HEIGHT = 50;

    /**
     * Agent będący właścicielem GUI.
     */
    private final TableAgent owner;
}
