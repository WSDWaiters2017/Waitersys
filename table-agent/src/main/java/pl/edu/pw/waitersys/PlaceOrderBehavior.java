package pl.edu.pw.waitersys;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import pl.edu.pw.waitersys.directory.WaiterDirectory;
import pl.edu.pw.waitersys.messages.OrderMessage;

import java.io.IOException;

/**
 * Jednorazowe zachowanie agenta stolika polegające na rozesłaniu kelnerom wybranego zamówienia.
 */
public class PlaceOrderBehavior extends OneShotBehaviour {
    /**
     * Tworzy zachowanie.
     *
     * @param order Zamówienie wybrane przez gości przy stoliku.
     */
    PlaceOrderBehavior(final Order order) {
        this.order = order;
    }

    /**
     * Wykonuje to zachowanie: wyszukuje dostępnych kelnerów i jeżeli jacyś są w lokalu,
     * informuje ich o wybranym zamówieniu za pośrednictwem wiadomości typu [[OrderMessage]].
     *
     * W przypadku błędu komunikacji, poddaje się bezsilnie.
     */
    public void action() {
        // 1. Wyszukaj zarejestrowanych kelnerów.
        final AID[] waiters = WaiterDirectory.findWaiters(myAgent);

        // 2. Jeżeli nie ma żadnych kelnerów, oburz się i zamachaj ramionami.
        if (waiters.length == 0) {
            System.err.println("Nie znaleziono żadnych kelnerów!");
            return;
        }

        // 3. Roześlij do wszystkich swoje zamówienie. Jeżeli się nie uda, oburz się.
        try {
            myAgent.send(new OrderMessage(order, waiters));
        } catch (IOException exc) {
            System.err.println("Stolik " + myAgent.getName() + " stracił łączność!");
        }
    }

    private final Order order;
}
