package pl.edu.pw.waitersys;

/**
 * Definiuje potrawy dostępne w lokalu.
 *
 * Każda potrawa posiada czytelną dla człowieka nazwę.
 */
public enum Food {
    SPAGHETTI_BOLOGNESE ("Spaghetti Bolognese"),
    BEEF_BURGER         ("Beef burger");

    /**
     * Zwraca czytelną nazwę potrawy.
     */
    public String readableName() {
        return rName;
    }

    Food(final String readableName) {
        this.rName = readableName;
    }

    private final String rName;
}
