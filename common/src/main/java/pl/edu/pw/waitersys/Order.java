package pl.edu.pw.waitersys;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Reprezentuje zamówienie.
 *
 * Zamówienie rozumiemy po prostu jako listę potraw.
 */
public class Order implements Serializable {
    /**
     * Konstruuje zamówienie zawierające wybrane potrawy. Może być puste.
     *
     * @param foods Potrawy wchodzące w skład zamówienia.
     */
    public Order(Food... foods) {
        positions = Arrays.asList(foods);
    }

    /**
     * Dodaje nową potrawę do zamówienia.
     *
     * Potrawa może się pokrywać z którąś z istniejących pozycji - wówczas zostanie
     * dostarczona wielokrotnie.
     *
     * @param meal Potrawa dopisywana do zamówienia.
     */
    public void addPosition(final Food meal) {
        positions.add(meal);
    }

    /**
     * Zwraca listę pozycji zamówienia.
     */
    public List<Food> getPositions() {
        return positions;
    }

    /**
     * Usuwa pojedyncze wystąpienie podanej potrawy z zamówienia.
     * Jeżeli zamówienie nie zawiera wybranej potrawy, pozostaje niezmienione.
     */
    public void removePosition(final Food meal) {
        positions.remove(meal);
    }

    private final List<Food> positions;
}
