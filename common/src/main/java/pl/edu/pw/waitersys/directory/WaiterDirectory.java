package pl.edu.pw.waitersys.directory;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.Arrays;

/**
 * Centralny katalog kelnerów, za pomocą którego kelnerzy mogą informować stoliki o swojej
 * dostępności, a stoliki mogą wyszukiwać dostępnych kelnerów.
 *
 * Udostępnia tylko metody statyczne, bo jest jedynie prostą fasadą do komunikacji z agentem DF.
 */
public class WaiterDirectory {
    /**
     * Rejestruje kelnera w katalogu. Do momentu wyrejestrowania, będzie on widoczny dla stolików.
     *
     * Metoda nie sprawdza, czy agent rzeczywiście potrafi obsługiwać zamówienia - zarejestrowanie
     * nie-kelnera prawdopodobnie skończy się błędem lub cichą akceptacją przy próbie komunikacji.
     *
     * @param agent Referencja do rejestrowanego agenta.
     */
    public static void announce(Agent agent) {
        final AID aid     = agent.getAID();
        final String name = aid.getName();

        final DFAgentDescription desc = makeWaiterDescription();
        desc.setName(aid);

        try {
            DFService.register(agent, desc);
            System.out.println("Zarejestrowano kelnera: " + name);
        } catch (FIPAException exc) {
            System.err.println("Nie udało się zarejestrować kelnera: " + name);
            exc.printStackTrace();
        }
    }

    /**
     * Wyrejestrowuje kelnera z katalogu.
     *
     * @param agent Kelner, który powinien zostać wyrejestrowany.
     */
    public static void renounce(Agent agent) {
        try {
            DFService.deregister(agent);
        } catch (FIPAException exc) {
            System.err.println("Nie udało się wyrejestrować kelnera: " + agent.getName());
            exc.printStackTrace();
        }
    }

    /**
     * Zwraca tablicę identyfikatorów wszystkich zarejestrowanych obecnie w katalogu kelnerów.
     *
     * @param agent Referencja do agenta odpytującego.
     * @return Tablica identyfikatorów zarejestrowanych kelnerów.
     */
    public static AID[] findWaiters(Agent agent) {
        final DFAgentDescription desc = makeWaiterDescription();

        try {
            return Arrays.stream(
                DFService.search(agent, desc)
            ).map(DFAgentDescription::getName).toArray(AID[]::new);
        } catch (FIPAException exc) {
            System.err.println("Nie udało się pobrać listy zarejestrowanych kelnerów!");
            exc.printStackTrace();
            return new AID[0];
        }
    }

    /**
     * Tworzy standardowy opis katalogowy dla kelnera, identyczny dla wszystich.
     *
     * Uwaga: Nie ustawia identyfikatora agenta! Jeżeli chcemy użyć opisu do rejestracji
     *        w katalogu, należy na zwróconym opisie wywołać metodę setName(agent)!
     */
    private static DFAgentDescription makeWaiterDescription() {
        final DFAgentDescription desc    = new DFAgentDescription();
        final ServiceDescription service = new ServiceDescription();
        service.setType("order-serving");
        service.setName("waitersys-order-serving");
        desc.addServices(service);

        return desc;
    }
}
